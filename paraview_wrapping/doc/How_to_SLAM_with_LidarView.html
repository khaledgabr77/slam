<!-- File auto-generated from How_to_SLAM_with_LidarView.md using https://www.browserling.com/tools/markdown-to-html -->
<h1 id="howtoslamwithlidarview">How to SLAM with LidarView ?</h1>

<ul>
  <li><a href="#installing-lidarview-or-one-of-its-derivative-with-slam-support">Installing LidarView or one of its derivative with SLAM support</a></li>
  <li><a href="#using-slam-in-lidarview">Using SLAM in LidarView</a></li>
  <li><a href="#saving-and-exporting-slam-outputs">Saving and exporting SLAM outputs</a>
  <ul>
    <li><a href="#saving-trajectory">Saving trajectory</a></li>
    <li><a href="#saving-keypoints-maps">Saving keypoints maps</a></li>
    <li><a href="#saving-aggregated-frames">Saving aggregated frames</a></li>
  </ul></li>
  <li><a href="#slam-parameters-tuning">SLAM parameters tuning</a></li>
</ul>

<p>This document presents some tips on how to use SLAM algorithm in LidarView, or one of its derived distribution. Even if this SLAM is embedded in a Paraview plugin and is therefore directly usable in Paraview, we will focus on its use in LidarView (as we consider here LiDAR data, LidarView  seems a better choice for most use-cases and display).</p>

<p>Since 2020, this SLAM plugin is natively included and available in LidarView.</p>

<h2 id="installinglidarvieworoneofitsderivativewithslamsupport">Installing LidarView or one of its derivative with SLAM support</h2>

<p>Follow <a href="https://gitlab.kitware.com/LidarView/lidarview-core/-/blob/master/Documentation/LidarView_Developer_Guide.md">LidarView's Developer Guide</a> instructions to build LidarView on Windows or Linux from source.</p>

<p><em><strong>IMPORTANT</strong>: to enable SLAM support, ensure  your CMake configuration has these options set to <code>True</code> :</em></p>
<pre><code>
-DENABLE_ceres=True
-DENABLE_nanoflann=True
-DENABLE_pcl=True
-DENABLE_slam=True 
</code></pre>

<p><code>LidarSlamPlugin</code> should be automatically loaded at LidarView's startup. If not, ensure <strong>Advanced features</strong> are enabled in <strong>Help</strong> tab, then select <strong>Tools</strong> > <strong>Manage Plugins</strong> > <strong>Load New</strong>. Browse to your LidarView install directory and select the <code>libLidarSlamPlugin.so</code> (this file can normally be found under <code>&lt;lidarview_superbuild_dir&gt;/install/lib/lidarview-3.6/plugins/libLidarSlamPlugin.so</code>).</p>

<h2 id="usingslaminlidarview">Using SLAM in LidarView</h2>

<p>LidarView's SLAM has been tested on <code>.pcap</code> files aquired from different LiDAR sensors including :</p>
<ul>
  <li>Velodyne VLP-16</li>
  <li>Velodyne VLP-32c</li>
  <li>Velodyne HDL-32</li>
  <li>Velodyne HDL-64</li>
  <li>Velodyne VLS-128</li>
</ul>

<ol>
  <li>Open LidarView. Make sure <strong>Advanced Features</strong> are enabled in <strong>Help</strong> tab.
  <p><img src="enable_advance_feature.png" alt="Enable advance feature" /></p></li>

  <li>Under <strong>Views</strong> tab, enable <strong>Pipeline Browser</strong> and <strong>Properties</strong>.
  <p><img src="enable_views_panels.png" alt="Enable views panels" /></p></li>

  <li><p>Open a previously recorded <code>.pcap</code> file (or set up a stream source) associated with its LiDAR calibration file.</p></li>

  <li><p>In <strong>Pipeline browser</strong>, select <strong>Frame</strong> (the pointcloud source). Then click on <strong>Filters</strong> tab > <strong>Alphabetical</strong> > <strong>SLAM</strong>.</p>
  <p><em><strong>Tip</strong> : After having selected <strong>Frame</strong> , you can also hit <code>Ctrl+space</code> and then type <code>slam</code> in filter search bar.</em></p>
  <p><img src="create_slam_filter.png" alt="Create SLAM filter" /></p></li>

  <li><p>Hit <code>Enter</code> to select a SLAM filter: pick <strong>SLAM (online)</strong> to perform a real-time test with live display, or <strong>SLAM (offline)</strong> for a full process, displaying only final trajectory and maps.</p></li>
  <li><p>A new input dialog will appear :</p>
  <ul>
    <li>Click on the <strong>Point Cloud</strong> input port, select the <strong>Frame</strong> entry. </li>
    <li>Click on the <strong>Calibration</strong> input port, select the <strong>Calibration</strong> entry. </li>
    <li>Hit <strong>OK</strong> when done.</li>
  </ul>
  <p><img src="select_slam_filter_inputs.png" alt="Select SLAM filter inputs"/></p></li>

  <li><p>Under <strong>Properties</strong> panel, modify the parameters if needed (see section <a href="#slam-parameters-tuning">SLAM parameters tuning</a>), then hit <strong>Apply</strong>.</p>
  <ul>
    <li>If you chose online SLAM, a white frame will appear. Hit play button to play back data through the entire recording and watch it SLAM in real time.</li>
    <li>If you chose offline SLAM, nothing new will show up after you hit <strong>Apply</strong>, but that's normal : the computer is working hard to run SLAM on all frames. When the processing is done, it will display its results.</li>
  </ul>
  </li>
</ol>

<h2 id="savingandexportingslamoutputs">Saving and exporting SLAM outputs</h2>

<h3 id="savingtrajectory">Saving trajectory</h3>

<p>Once SLAM is complete, you can export the Trajectory (for example as as <code>.poses</code> file) to avoid running the SLAM again. To save it, select in the <strong>Pipeline Browser</strong> panel the <strong>Trajectory</strong> output, then hit <code>Ctrl+s</code> (or <strong>Advance</strong> tab > <strong>File</strong> > <strong>Save Data</strong>), and choose the output format and name in the dialog window. Later, to load the trajectory back in LidarView, you can drag and drop the <code>.poses</code> file.</p>

<h3 id="savingkeypointsmaps">Saving keypoints maps</h3>

<p>To save SLAM keypoints maps, select the map output you want to save in the <strong>Pipeline Browser</strong> panel, then hit <code>Ctrl+s</code> (or <strong>Advance</strong> tab > <strong>File</strong> > <strong>Save Data</strong>), and choose the output format and name in the dialog window. Common pointclouds formats are <code>csv</code>, <code>pcd</code>, <code>las</code>, <code>ply</code> or <code>vtp</code>.</p>

<h3 id="savingaggregatedframes">Saving aggregated frames</h3>

<p>To export processed frames as a single aggregated pointcloud, you need to instanciate a <strong>Transforms Applier</strong> filter to aggregate all frames using the computed trajectory (sensor path estimated by SLAM):</p>

<ol>
  <li>Select the <strong>Trailing frame</strong> entry and set the desired number of trailing frames (0 meaning only the last frame, and, for example, 10 displaying the current frame and the 10 previous ones). Click on <strong>Apply</strong>. You should now see all the frames aggregated in a non-sense way (all points being displayed using their coordinates relative to the sensor at the time of acquisition).</li>
  <li>Instantiate a <strong>Temporal Transform Applier</strong> filter using the Trailing Frame as point cloud entry, and the output SLAM trajectory for trajectory entry. Depending on the number of trailing frames, the transformation and aggregation of pointclouds may be long. When it succeeds, you should now see all points being correctly registered. If the colors look strange, check that you are displaying the <code>intensity</code> array in the main toolbar.</li>
  <li>As usual, save aggregated frames by selecting the desired output <strong>Temporal Transform Applier</strong>, hit <code>Ctrl+s</code>, and choose the output format and name.</li>
</ol>
<p><img src="aggregated_frames.png" alt="Aggregated frames"/></p>

<h2 id="slamparameterstuning">SLAM parameters tuning</h2>

<p>The default SLAM parameters are a good compromise to run the SLAM in outdoor urban area, indoor scene and poor geometric scene (forest recorded from UAV, glades, career, ...). However, the parameters can be adapted to the specific kind of environment you want to process to have an optimal result. Especially, consider tuning these parameters first :</p>

<ul>
  <li><p><strong><em>General parameters</em></strong></p>
    <ul>
      <li><strong>Number of threads</strong> : maximum number of threads used for parallel processing. Allowing several threads (about 4) increase SLAM processing speed, skipping less frames, and thus improving result.</li>
      <li><strong>Undistortion mode</strong> : undistortion greatly improves precision if correctly performed. This implementation makes the assumption that motion is continuous and approximately constant. If this is not true (especially for high-frequency moving plateforms such as UAVs) or experiencing noisy jumps, consider turning it OFF.</li>
      <li><strong>Ego-Motion mode</strong> : current frame registration on environment map needs a good optimization initialization to converge. This initialization is done by estimating ego-motion since last frame. Default mode uses continuous and approximately constant speed motion hypothesis to interpolate new pose. If these assumptions are not true, try registration of current frame on previous frame.</li>
      <li><strong>Fast SLAM</strong> : SLAM uses by default only edges and planes keypoints. However, if scene presents a poor geometric environment, results could be badly impacted. In this case, consider disabling <em>Fast Slam</em> option to allow the use of blobs keypoints.</li>
    </ul>
  </li>

  <li><p><strong><em>Maps parameters</em></strong></p>
    <ul>
      <li><strong>Edges/Planes/Blobs map resolution</strong> : SLAM maps are voxelized to reduce memory consumption and problem dimensionality to keep at most one point in each cube of a given size. Default resolution (30 cm for edges or blobs keypoints, 60 cm for planes) is good for outdoor structured environments, but a more precise resolution may help in indoor or short-range scenes.</li>
    </ul>
  </li>

  <li><p><strong><em>Spinning sensor keypoints extractor parameters</em></strong></p>
    <ul>
      <li><strong>Neighborhood width</strong> : it can be useful to lower [increase] this number if you want more high level [local] features. Typically, a higher value may benefit to a high azimuthal resolution sensor (remember also to set <strong>Azimutal angle resolution</strong> according to your sensor specifications).</li>
      <li><strong>Minimum distance to sensor</strong> : points closer to the sensor than this distance are ignored. In an indoor scene or with a man-held LiDAR, this parameter may typically need a smaller value.</li>
    </ul>
  </li>
</ul>